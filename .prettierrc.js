module.exports = {
  semi: true,
  tabWidth: 2,
  singleQuote: true,
  bracketSpacing: true,
  printWidth: 120,
  jsxSingleQuote: false,
  jsxBracketSameLine: true,
  trailingComma: 'none'
};
