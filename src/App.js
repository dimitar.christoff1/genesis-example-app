import * as React from 'react';

const customRules = {
  ticker: {
    format: {
      pattern: '^(?<PreXChangeCode>[a-z]{2,5}:(?![a-z\\d]+\\.))?(?<Stock>[a-z]{1,5}|\\d{1,3}(?=\\.)|\\d{4,})(?<PostXChangeCode>\\.[a-z]{2})?$',
      flags: 'i',
      message: '^Invalid %{value} - Please a valid ticker, eg. GOOGL or GOOG.BY or 98774'
    }
  }
};


function App() {

  return (
    <div className="container">
      <div className="columns">
        <div className="column">
          <gui-form onGuiFormSubmit={console.log} rules={customRules} onGuiFormValidationErrorg={console.log}>
            <gui-input type="text" id="name" name="name" label="Name" rules="between-5-15"/>
            <gui-input type="text" id="email" name="email" label="Email" rules="email"/>
            <gui-input type="text" id="source" name="source" label="How did you hear about us" rules="required"/>
            <gui-input
              type="text"
              name="other"
              id="other"
              label="Details"
              rules="combo-source=other"
              data-helper-text="^Please provide details when source is other"
            />

            <p className="buttons">
              <gui-button submit="true">Send</gui-button>
            </p>
          </gui-form>
        </div>
        <div className="column">
          <gui-form onGuiFormSubmit={console.log} rules={customRules} onGuiFormValidationErrorg={console.log}>
            <gui-input type="text" id="gui-bank-username-legacy" name="gui-bank-username-legacy" label="Username"
                       rules="between-5-12"/>
            <gui-input type="text" id="isin" name="isin" label="ISIN" rules="isin" value="US45256BAD38"/>
            <gui-input type="text" id="ticker" name="ticker" label="Ticker" rules="ticker" value=""/>

            <p className="buttons">
              <gui-button submit="true">Send</gui-button>
            </p>
          </gui-form>
        </div>
      </div>

    </div>
  );
}

export default App;
