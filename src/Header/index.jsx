import { useState, useEffect, useRef } from 'react';
import { NavLink as Link } from 'react-router-dom';
import logo from './logo.svg';

function LinkRenderer({ label, ...rest }) {
  return (
    <li className={'dropdown'}>
      <Link {...rest} activeClassName={'active'}>
        {label}
      </Link>
    </li>
  );
}

const NavBar = ({ items }) => {
  return (
    <div className="navigation" slot={'nav'}>
      <ul>
        {items.map((link) => (
          <LinkRenderer {...link} key={link.label} />
        ))}
      </ul>
    </div>
  );
};

function Header() {
  const [darkTheme, setDarkTheme] = useState(localStorage.getItem('dark-theme') === 'true');

  const header = useRef();

  useEffect(() => {
    if (header.current) {
      header.current.addEventListener('guiChange', () => setDarkTheme(!darkTheme));
    }
  }, [header]);

  useEffect(() => {
    localStorage.setItem('dark-theme', darkTheme.toString());
    document.body.classList[darkTheme ? 'add' : 'remove']('dark');
  }, [darkTheme]);

  return (
    <gui-header logo={logo} theme-toggle={true} class="App-header" ref={header}>
      <NavBar
        items={[
          {
            exact: true,
            to: '/',
            label: 'Form validation'
          },
          {
            to: '/charts',
            label: 'Charts'
          }
        ]}
      />
    </gui-header>
  );
}

export default Header;
