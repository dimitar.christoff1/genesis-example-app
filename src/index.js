import React from 'react';
import ReactDOM from 'react-dom';
import { defineCustomElements } from '@genesisglobal/genesis-ui/loader/index.cjs.js';
import "@genesisglobal/genesis-ui/dist/genesis-ui/genesis-ui.css";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './index.scss';

import App from './App';
import Charts from './Charts';
import Header from './Header';

import reportWebVitals from './reportWebVitals';

defineCustomElements(window);

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Header />
      <Switch>
        <Route exact path={'/'}>
          <App />
        </Route>
        <Route exact path={'/charts'}>
          <Charts />
        </Route>
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
