import * as React from 'react';

function Charts() {
  return (
    <div className="container">
      <h1>Charts</h1>
      <div className="columns">
        <div className="column" style={{minHeight: 400}}>
          <gui-column-chart
            id="columnChart"
            series='[{"id":"s1", "label":"Visits", "field":"visits"}, {"id":"s2", "label":"Population", "field":"population"}]'
            category="country">
            <gui-simple-data
              data='[{
                        "country": "USA",
                        "visits": 2025,
                        "population": 4000
                    }, {
                        "country": "China",
                        "visits": 1882,
                        "population": 2500
                    }, {
                        "country": "Japan",
                        "visits": 1809,
                        "population": 3982
                    }, {
                        "country": "Germany",
                        "visits": 1322,
                        "population": 2200
                    }, {
                        "country": "UK",
                        "visits": 1122,
                        "population": 1500
                    }, {
                        "country": "France",
                        "visits": 1114,
                        "population": 3000
                    }, {
                        "country": "India",
                        "visits": 984,
                        "population": 3500
                    }]'></gui-simple-data>
          </gui-column-chart>
        </div>
        <div className="column">
          <gui-column-chart
            id="columnChart"
            series='[{"id":"s1", "label":"Visits", "field":"visits"}, {"id":"s2", "label":"Population", "field":"population"}]'
            category="country">
            <gui-simple-data
              data='[{
                        "country": "USA",
                        "visits": 2025,
                        "population": 4000
                    }, {
                        "country": "China",
                        "visits": 1882,
                        "population": 2500
                    }, {
                        "country": "Japan",
                        "visits": 1809,
                        "population": 3982
                    }, {
                        "country": "Germany",
                        "visits": 1322,
                        "population": 2200
                    }, {
                        "country": "UK",
                        "visits": 1122,
                        "population": 1500
                    }, {
                        "country": "France",
                        "visits": 1114,
                        "population": 3000
                    }, {
                        "country": "India",
                        "visits": 984,
                        "population": 3500
                    }]'></gui-simple-data>
          </gui-column-chart>
        </div>
      </div>

    </div>
  );
}

export default Charts;
